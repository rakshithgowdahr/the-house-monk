import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Search from "./containers/Search/Search";
import Profile from "./containers/Profile/Profile";
import { getUsers } from "./services/usersServices";
import "./App.css";

class App extends Component {
  state = {
    users: [],
    filter: {
      name: ""
    }
  };
  componentDidMount() {
    const state = { ...this.state };
    state.users = getUsers();
    this.setState(state);
  }
  handleInputChange = e => {
    const filter = { ...this.state.filter };
    filter.name = e.target.value;
    this.setState({ filter });
  };
  clearSearchFilter = () => {
    const filter = { ...this.state.filter };
    filter.name = "";
    this.setState({ filter });
  };
  render() {
    const { users, filter } = this.state;
    let filtered = users;
    if (filter.name) {
      filtered = users.filter(m =>
        m.name.toLowerCase().startsWith(filter.name.toLowerCase())
      );
    }
    return (
      <React.Fragment>
        <Switch>
          <Route
            path="/users"
            exact
            render={() => (
              <Search
                users={filtered}
                filter={filter}
                bindSearchText={this.handleInputChange}
                clearFilter={this.clearSearchFilter}
              />
            )}
          />
          <Route path="/users/:id" render={() => <Profile />} />
          <Redirect from="/" exact to="/users" />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
