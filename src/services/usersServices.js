const users = [
  {
    id: 1,
    name: "User Name",
    username: "@handbook",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas assumenda.",
    worksAt: "Lorem ipsum dolor Pvt. Ltd",
    repositories: 15,
    followers: 250,
    pinnedRepositories: [
      {
        name: "Repo Name",
        username: "reponame",
        description: "Repo Description"
      },
      {
        name: "Repo Name",
        username: "reponame",
        description: "Repo Description"
      },
      {
        name: "Repo Name",
        username: "reponame",
        description: "Repo Description"
      }
    ]
  },
  {
    id: 2,
    name: "Test Name",
    username: "@handbook",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas assumenda.",
    worksAt: "Lorem ipsum dolor Pvt. Ltd",
    repositories: 12,
    followers: 2520,
    pinnedRepositories: [
        {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          }
    ]
  },
  {
    id: 3,
    name: "Xyz Name",
    username: "@handbook",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas assumenda.",
    worksAt: "Lorem ipsum dolor Pvt. Ltd",
    repositories: 23,
    followers: 600,
    pinnedRepositories: [
        {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          }
    ]
  },
  {
    id: 4,
    name: "User Name",
    username: "@handbook",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas assumenda.",
    worksAt: "Lorem ipsum dolor Pvt. Ltd",
    repositories: 18,
    followers: 122,
    pinnedRepositories: [
        {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          }
    ]
  },
  {
    id: 5,
    name: "User Name",
    username: "@handbook",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas assumenda.",
    worksAt: "Lorem ipsum dolor Pvt. Ltd",
    repositories: 75,
    followers: 320,
    pinnedRepositories: [
        {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          }
    ]
  },
  {
    id: 6,
    name: "User Name",
    username: "@handbook",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas assumenda.",
    worksAt: "Lorem ipsum dolor Pvt. Ltd",
    repositories: 75,
    followers: 750,
    pinnedRepositories: [
        {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          },
          {
            name: "Repo Name",
            username: "reponame",
            description: "Repo Description"
          }
    ]
  }
];

export function getUsers() {
  return users;
}
export function getUser(id) {
  return users.find(user => user.id === id);
}
