import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faTimes } from "@fortawesome/free-solid-svg-icons";
import logo from "../../assets/images/logo.png";
import user from "../../assets/images/user.png";
import "./Search.css";

function Search(props) {
  let history = useHistory();
  function redirectUser(id) {
    history.push(`/users/${id}`, { id: id });
  }
  let cards = [];
  if (props.users.length) {
    props.users.forEach(item => {
      cards.push(
        <div key={item.id} className="col-md-4">
          <div
            style={{ cursor: "pointer" }}
            onClick={() => redirectUser(item.id)}
            className="card"
          >
            <div className="row">
              <div className="col-md-3">
                <img className="user" src={user} alt="User" />
              </div>
              <div className="col-md-8">
                <h5 style={{ margin: "15px 0 0 0" }}>{item.name}</h5>
                <p
                  style={{
                    fontSize: "14px"
                  }}
                >
                  {item.username}, {item.description}
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }
  return (
    <div>
      <div className="search-container">
        <div className="logo-container">
          <img className="logo" alt="GitHub" src={logo} />
          <p className="heading">GitHub Profile Viewer</p>
        </div>
        <div className="search-bar">
          <FontAwesomeIcon icon={faSearch} className="search" />
          <FontAwesomeIcon
            onClick={props.clearFilter}
            icon={faTimes}
            className="search-1"
          />
          <input
            onChange={e => props.bindSearchText(e)}
            value={props.filter.name}
            className="input"
            placeholder="Search user"
            type="text"
          />
        </div>
      </div>
      <div className="card-container">
        <div className="row">{cards}</div>
      </div>
    </div>
  );
}

export default Search;
