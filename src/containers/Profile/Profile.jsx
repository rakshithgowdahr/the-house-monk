import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReply } from "@fortawesome/free-solid-svg-icons";
import { getUser } from "../../services/usersServices";
import profile from "../../assets/images/profile.png";
import repo from "../../assets/images/user.png";
import "./Profile.css";

function Profile(props) {
  let history = useHistory();
  const [user, setUser] = useState({});
  useEffect(() => {
    setUser(getUser(history.location.state.id));
  }, [user]);
  function redirectHome() {
    history.push("/users");
  }
  let cards = [];
  if (user.pinnedRepositories) {
    user.pinnedRepositories.forEach((item, index) => {
      cards.push(
        <div key={index} className="row">
          <div className="col-md-12">
            <div className="card1">
              <div className="row">
                <div className="col-md-1">
                  <img className="user" src={repo} alt="User" />
                </div>
                <div className="col-md-6">
                  <p
                    style={{
                      fontWeight: "bold",
                      margin: "10px 0px 0px 0px"
                    }}
                  >
                    {`${user.username}/${item.username}`}
                  </p>
                  <p style={{ margin: "0 0 0 0" }}>{item.description}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }
  return (
    <div>
      <div className="search-container">
        <div
          style={{ cursor: "pointer" }}
          onClick={redirectHome}
          className="back"
        >
          <FontAwesomeIcon className="back-icon" icon={faReply} />
          <p className="back-text">Back</p>
        </div>
        <div className="logo-container1">
          <img className="profile" alt="GitHub" src={profile} />
          <div className="user-name">
            <h4 style={{ marginBottom: "0" }}>{user.name}</h4>
            <p style={{ margin: "0" }}>{user.username}</p>
          </div>
        </div>
      </div>
      <div
        style={{ paddingLeft: "10px", paddingTop: "0" }}
        className="card-container1"
      >
        <div className="wrap">
          <div className="row">
            <div className="col-md-12">
              <h6>Bio</h6>
              <p>{user.description}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <h6>Works At</h6>
              <p>{user.worksAt}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <h6>Repositories</h6>
              <p>{user.repositories}</p>
            </div>
            <div className="col-md-6">
              <h6>Followers</h6>
              <p>{user.followers}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <h6>Pinned Repositories</h6>
              {cards}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
